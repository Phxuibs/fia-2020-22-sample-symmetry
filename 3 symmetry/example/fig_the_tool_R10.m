%% fig_the_tool_R10 %%

% Generates a simple sample consisting of hemispheres and run the whole
% process to calculate the symmetry function.

% R = turntable radius
% r = hemisphere radius
% theta = vector with angular polar coordinates (rad)
% rho = vector with distance to center polar coordinates
close all
clear
clc

sample.R = 1;
sample.r = sample.R/10;
sample.theta = [0 0 pi/4 pi 3/2*pi];
sample.rho = [0.05 0.8 0.5 0.8 0.3];
%%
sample = utf_symmetry_coefficient(sample);
%%
utf_symmetry_coefficient_plot(sample,"fig_the_tool_R10","all",32,33.5);