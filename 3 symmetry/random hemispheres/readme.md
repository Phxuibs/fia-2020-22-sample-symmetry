The virtual samples were generated as explained in the paper.
The symmetry functions were calculated in the same way as in the code file in the "example" folder (just go back one level), i.e. using the ``utf_symmetry_coefficient`` and ``utf_symmetry_coefficient_plot`` functions from the [UTF toolbox](https://gitlab.com/Phxuibs/utf-toolbox.git).
