## Important note

Sample C was measured twice (this was the "repetitivity test" mentioned in the paper), so you'll find references to samples "C1" and "C2" - they are the same, with just different measurement dates (detailed below). Sample C2 was selected to showcase in the paper.

## If you just want the raw results

To get the raw values of all coefficients, go to the "results" folder, load the `.mat` file corresponding to the sample of your interest and see the `coef` object. The uncertainties present the `u_` prefix (with `u_c` standing for combined uncertainty). You may multiply these values by 2 to obtain the 95% confidence level (as done for generating the plots in the paper).

## Common terms

Some words were not translated:
- "amostra" is portuguese for "sample" (referring to the hemispheres composed samples);
- "vazia" is portuguese for "empty" (meaning an empty reverberation room, i.e. without the sample, for T1 and T3 measurements);

`t_h` is a common suffix for files; it means "temperature and humidity".

## Measurement dates

- Samples A and B were measured on **October 16 2021**;
- Sample C1 was measured on **October 20 2021**;
- Samples A, B and C1 have the same measurements for T1 and T3 (from **October 16**) - they were just copied and pasted on different folders for easier coding;
- Sample C2 was measured on **April 19 2022**;
- Sample's D T2 and T4 were measured on same day as sample C2 (**April 19**);
- Sample's D T1 and T3 were measured on **May 28 2022**.

## Need any help?

Don't hesitate to email luis.2014@alunos.utfpr.edu.br! ;)
