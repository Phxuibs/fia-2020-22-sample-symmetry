%% scattering coefficient measurements %%

clear
clc
% for T1 and T2, fftDegree = 18, mean out of 05 runs e stopMargin = 0.3;
% for T3 and T4, fftDegree = 16, mean out of 72 runs e stopMargin = 0.3;
% measurement setup
MS = utf_ITA_MS('input',[1,2],'freqRange',[500 16000],'fftDegree',16,...
                'numAverages',72,'stopMargin',0.3,'latencySamples',623);
%%
clc
% measurement name
M = "vazia_com_fita_800";
% measurement step
T = 3;
% position
P = 6;
tic
% run the measurement
result = MS.run;
toc
% save measurement
ita_write(result,M+"_T"+T+"_P"+P+".ita");
