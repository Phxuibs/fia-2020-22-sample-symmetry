%% plots - absorption coefficients %%

close all
clear
clc

% load results
path = "results/";
sample_A = load(path+"A.mat");
sample_A = sample_A.coef;
sample_B = load(path+"B.mat");
sample_B = sample_B.coef;
sample_C = load(path+"C2.mat");
sample_C = sample_C.coef;
sample_D = load(path+"D.mat");
sample_D = sample_D.coef;
% frequency vector
f = utf_center_third_octave_bands([500 16000]);
%% plot
h = figure();
hold on
errorbar(f,sample_A.alpha_s,2*sample_A.u_c_alpha_s,'-o',...
         'linewidth',2,'color',utf_color("azul_claro"),'markerFaceColor','auto');
errorbar(f,sample_B.alpha_s,2*sample_B.u_c_alpha_s,'-s',...
         'linewidth',2,'color',utf_color("verde"),'markerFaceColor','auto');
errorbar(f,sample_C.alpha_s,2*sample_C.u_c_alpha_s,'-^',...
         'linewidth',2,'color',utf_color("laranja"),'markerFaceColor','auto');
errorbar(f,sample_D.alpha_s,2*sample_D.u_c_alpha_s,'-v',...
         'linewidth',2,'color',utf_color("vermelho"),'markerFaceColor','auto');
yline(0.5,'--','linewidth',2,'color',[0 0 0]);
hold off
ax = utf_axes(gca,32);
ax.XScale = 'log';
ax.XTick = f;
xlabel("Frequency (Hz)");
ylabel("$\alpha_\mathrm{s}\quad(\cdot)$");
lgd = legend(["A","B","C","D"],'interpreter','latex','location','northwest');
lgd.Title.String = "Sample";
lgd.Title.Interpreter = 'latex';
ax = utf_minor_grid(ax);
ax.XMinorTick = 'off';
ax.XMinorGrid = 'off';
ylim([0 1]);
utf_adjust_plot_fine(h,"fig_alpha_s",[33.5 19]);