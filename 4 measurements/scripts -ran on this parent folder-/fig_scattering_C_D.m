%% plots - scattering of samples C2 and D %%

%% setup
close all
clear
clc

% names of .mat files with the coefficients
sample = ["C2","D"];
% path to files
path = "results/";
legenda = ["C","D"];
% Lambda (hemispheres radii)
Lambda = [0.02 0.02];
% markers and colors
markers = ["-^","-v"];
colors = [utf_color("vermelho"),utf_color("indigo")];
% coefficient
coef = "s";
% frequency vector
f = utf_center_third_octave_bands([500 16000]);
%% plot
h = figure();
hold on
for i = 1 : length(sample)
    % load results
    results = load(path+sample(i)+".mat");
    % plot
    eval("errorbar(Lambda(i)*f/343,results.coef."+coef+",results.coef.u_c_"+coef+"*2,"+...
         "markers(i),'linewidth',2,'color',colors(:,i),'markerFaceColor',colors(:,i));");
end
hold off
ax = gca;
ax.XScale = 'log';
ax = utf_axes(ax,32);
ax.XTick = [0.02 0.1 0.2:0.2:1];
xlim([0.02 1]);
ylim([-0.1 1]);
ax.YTick = 0:0.1:1;
ax.XMinorTick = 'on';
ax.XMinorGrid = 'on';
L = legend(legenda,'Location','northwest');
L.Title.String = "Sample";
L.Interpreter = 'latex';
xlabel("$r/\lambda\ (\cdot)$");
ylabel("$s\ (\cdot)$");
utf_adjust_plot_fine(h,"fig_C_D",[33.5 19])