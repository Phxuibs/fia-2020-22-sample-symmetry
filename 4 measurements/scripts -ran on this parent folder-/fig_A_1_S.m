%% plots - A_1 / S ratio %%

close all
clear
clc
% load results
load("results/A.mat");
% absorption of empty room
A_1 = coef.A_1;
% standard uncertainty for A_1
u_A_1 = coef.u_A_1;
% frequency vector
f = utf_center_third_octave_bands([500 16000]);
% area for larger sample (whole turntable)
S = pi*(0.79/2)^2;
% area for smaller sample
s = pi*0.3^2;
% expanded uncertainty for A_1 / S
U_A_1_S = 2*sqrt(u_A_1.^2 * (1/S)^2);
% expanded uncertainty for A_1 / s
U_A_1_s = 2*sqrt(u_A_1.^2 * (1/s)^2);

%% plot
h = figure();
errorbar(f,A_1/S,U_A_1_S,'-o','linewidth',2,'color',utf_color("azul"),...
         'markerFaceColor','auto','markerSize',6);
hold on
errorbar(f,A_1/s,U_A_1_s,'-s','linewidth',2,'color',utf_color("verde_escuro"),...
         'markerFaceColor','auto','markerSize',6);
yline(1,'--','linewidth',3,'color',utf_color("vermelho"));
hold off
ax = utf_axes(gca,32);
ax.XScale = 'log';
ax.XTick = f;
ax = utf_minor_grid(ax);
ax.XMinorGrid = 'off';
ax.XMinorTick = 'off';
xlabel("Frequency (Hz)");
ylabel("$A_1/S\quad(\cdot)$");
lgd = legend(["$790\,\mathrm{mm}$","$600\,\mathrm{mm}$"],'interpreter','latex',...
       'location','northwest');
lgd.Title.String = "Sample diameter";
lgd.Title.Interpreter = 'latex';
utf_adjust_plot_fine(h,"fig_A_1_S",[33.5 19]);