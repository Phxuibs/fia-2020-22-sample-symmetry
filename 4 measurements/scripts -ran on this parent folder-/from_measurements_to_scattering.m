%% scattering from ground one %%

clear
clc
%% manual setup
% sample name (letter)
sample = "C1";
% relative path to parent folder of RIR folders and temperature and
% humidity .mat files
path = "measurements/";
%% automatic setup
% sample area (m^2)
if(sample=="A" || sample=="C1" || sample=="C2" || sample=="D")
    S = pi * (0.790/2)^2;
else
    if(isequal(sample,"B"))
        S = pi * (0.600/2)^2;
    end
end
% frequency range (Hz)
freqRange = [500 16000];
% frequency vector
f = utf_center_third_octave_bands(freqRange);
% number of source-receiver positions
M = 12;
% chamber volume (m^3)
V= 1.48;
%% load temperature and humidity
load(path + "amostra "+sample+" - t_h.mat");
%% TR estimation (without considering air absorption)
% reverberation time array allocation
trTot = nan([M length(f)]);
% calculates T1
disp("Calculando T1...")
trTot(:,:,1) = utf_TR_ITA('path',path+"amostra "+sample+" - vazia/",...
                          'snip',"T1",'freqRange',freqRange,'idxFlag',false);
% calculates T2
disp("Calculando T2...")
trTot(:,:,2) = utf_TR_ITA('path',path+"amostra "+sample+"/",...
                          'snip',"T2",'freqRange',freqRange,'idxFlag',false);
% calculates T3
disp("Calculando T3...")
trTot(:,:,3) = utf_TR_ITA('path',path+"amostra "+sample+" - vazia/",...
                          'snip',"T3",'freqRange',freqRange,'idxFlag',true);
% calculates T4
disp("Calculando T4...")
trTot(:,:,4) = utf_TR_ITA('path',path+"amostra "+sample+"/",...
                          'snip',"T4",'freqRange',freqRange,'idxFlag',true);
%% now considering air absorption
disp("Considerando absorção do ar...");
% allocate array
trSurf = nan(size(trTot));
for i = 1 : M
    for k = 1 : 4
        trSurf(i,:,k) = utf_T_surf(trTot(i,:,k),f,'T',t(i,k),'h_r',h(i,k));
    end
end
%% calculate coefficients (s, alpha_s, alpha_spec, s_base)
disp("Calculando os coeficientes...");
% reverberation time mean value and standard deviation
meanT = permute(mean(trSurf),[3 2 1]);
stdT = permute(std(trSurf),[3 2 1]);
% speed of sound
c = utf_speed_of_sound(mean(t));
% coefficients
coef = utf_coef(meanT,stdT,c,S,M,V);
%% salvar
% remove iteration variables
clearvars i k
disp("Salvando resultados...");
save(sample+".mat");
disp("Pronto.");