%% plots - air absorption %%

%% setup
close all
clear
clc
% for sample C2
load("results/C2.mat");
%% for T1
% air absorption (dB/m) (T1)
alpha_dB_m = utf_air_attenuation(f,mean(t(:,1)),mean(h(:,1)),1,1).';
% air absorption (dB/s) (T1)
decay_air_T1 = alpha_dB_m * utf_speed_of_sound(mean(t(:,1)));
% decay accounting air and surfaces (T1)
decay_total_T1 = 60 ./ mean(trTot(:,:,1),1);
%% for T2
% air absorption (dB/m) (T2)
alpha_dB_m = utf_air_attenuation(f,mean(t(:,2)),mean(h(:,2)),1,1).';
% air absorption (dB/s) (T2)
decay_air_T2 = alpha_dB_m * utf_speed_of_sound(mean(t(:,2)));
% decay accounting air and surfaces (T2)
decay_total_T2 = 60 ./ mean(trTot(:,:,2),1);
%% for T4
% air absorption (dB/m) (T4)
alpha_dB_m = utf_air_attenuation(f,mean(t(:,4)),mean(h(:,4)),1,1).';
% air absorption (dB/s) (T4)
decay_air_T4 = alpha_dB_m * utf_speed_of_sound(mean(t(:,4)));
% decay accounting air and surfaces (T4)
decay_total_T4 = 60 ./ mean(trTot(:,:,4),1);
clearvars -except decay_air_T4 decay_total_T4 decay_air_T2 decay_total_T2 decay_air_T1 decay_total_T1 f
%% plot
h = figure();
hold on
plot(f,decay_air_T1./decay_total_T1,'-o','linewidth',2,'markerSize',9,...
    'Color',utf_color("azul"),'markerFaceColor',utf_color("azul"));
plot(f,decay_air_T2./decay_total_T2,'-s','linewidth',2,'markerSize',10,...
    'Color',utf_color("verde"),'markerFaceColor',utf_color("verde"));
plot(f,decay_air_T4./decay_total_T4,'-^','linewidth',2,'markerSize',10,...
     'color',utf_color("vermelho"),'markerFaceColor',utf_color("vermelho"));
hold off
ax = gca;
ax.XScale = 'log';
ax = utf_axes(ax,32);
ax.XTick = f;
ax.YMinorTick = 'on';
ax.YMinorGrid = 'on';
% ax.XMinorTick = 'on';
% ax.XMinorGrid = 'on';
xlabel("Frequency (Hz)");
ylabel("$d_\mathrm{air} / d_\mathrm{total}\ (\cdot)$");
legend("Without sample ($T_1$)","With sample ($T_2$)","With rotating sample ($T_4$)",...
       'location','northwest','interpreter','latex');
utf_adjust_plot_fine(h,"fig_air_absorption",[33.5 19]);