%% plots - baseplate/turntable scattering %%

%% setup
close all
clear
clc

path = "results/";
sample = ["A","B","C1","C2","D"];
legenda = ["A","B","C1","C2","D"];
% markers and colors
markers = ["-o","-s","-^","-v","-P"];
colors = [utf_color("azul"),utf_color("verde"),utf_color("verde_escuro"),utf_color("vermelho"),utf_color("indigo")];
% coefficient
coef = "s_base";
% frequency vector
f = utf_center_third_octave_bands([500 16000]);
%% plot
h = figure();
s_base_limits = [repmat(0.05,[1 8]) 0.1 0.1 0.1 0.15 0.15 0.15 0.2 0.2];
hold on
for i = 1 : length(sample)
    % load results
    results = load(path+sample(i)+".mat");
    % plot
    eval("errorbar(f,results.coef."+coef+",results.coef.u_c_"+coef+"*2,"+...
         "markers(i),'linewidth',2,'color',colors(:,i),'markerFaceColor',colors(:,i));");
end
plot(f,s_base_limits,'-','linewidth',2,'color',[0 0 0]);
hold off
ax = gca;
ax.XScale = 'log';
ax = utf_axes(ax,32);
ax.XTick = f;
% ylim([-0.1 1]);
ax.YTick = 0:0.1:1;
ax.XMinorTick = 'on';
ax.XMinorGrid = 'on';
L = legend(legenda,'Location','northwest');
L.Title.String = "Sample";
L.Interpreter = 'latex';
xlabel("Frequency (Hz)");
ylabel("$s_\mathrm{base}\ (\cdot)$");
utf_adjust_plot_fine(h,"fig_s_base",[33.5 19]);