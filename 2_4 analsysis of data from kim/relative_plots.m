%% plots using data from Kim's paper [1] %%

% Plot the scattering coefficient data grouped by area coverage and using
% relative axis: X axis is element's radius over sound wavelength and Y
% axis points the ratio between the scattering coefficient for a sample at
% a frequency band over the maximum scattering coefficient in the whole
% group (same area coverage) minus 1 (the maximum value is then at 0).

% You might need to adjust the screenSize variable to generate the plots in
% the right size (see below)

% [1] Kim, Y. H. et al. Characterizing diffusive surfaces using scattering
% and diffusion coefficients. Applied Acoustics, 72: 899–905, 2011.

clear
clc
screenSize = [34 19]; % screen width and height, in centimeters
%% load the dataset
load('kim.mat'); 
% Explaining the dataset:
% cov: the present area coverages.
% data: scattering coefficient data consisting of five tables, one for
% each area coverage. The rows are for each element size and each column is
% for a frequency band.
% f: the present frequency bands.
% lambda: the corresponding sound wavelength.
% n: the estimated number of hemispheres in each sample. Rows correspond to
% element size while columns correspond to area coverage.
% r: the present hemispheres' radii.
% R: the turntable radius.
%% Plots!
close all
cor = ["azul","verde","laranja","vermelho"]; % colors for curves (names in portuguese)
cor = [cor cor]; % just doubling the vector, repeating the color order.
marker = [":v",":o",":s",":^","-v","-o","-s","-^"];
% initiating plotting
for j = 4:5 % the desired area coverages to be plotted (1:5)
    fig = figure(j);
    legenda = []; % empty legend to add automatized text later
    hold on
    s_max = max(data{1,j},[],'all'); % find the maximum scattering value at each group
        for i = 1 : numel(r)
            if(isequal(nnz(isnan(data{1,j}(i,:))),0)) % verify if there's data for radius r(i) and coverage cov(j)
                p(i) = plot(r(i)./lambda,data{1,j}(i,:)/s_max - 1,marker(i),... % <<<=== the plot per se!!!
                            'linewidth',2,'color',utf_color(cor(i)));
                if(i<4) % smaller elements present a slightly lighter curve color
                    p(i).Color = p(i).Color .* 1.15;
                end
                if(i>4) % larger elements have filled markers
                    p(i).MarkerFaceColor = utf_color(cor(i));
                end
                % the end of this inner loop writes the legend and adapts
                % it to be aligned
                R_r = R/r(i);
                if(R_r<10)
                    legenda = [legenda "$0"+sprintf('%0.2f',R_r)];
                else
                    legenda = [legenda "$ "+sprintf('%0.2f',R_r)];
                end
                if(round(n(i,j))<100)
                    legenda(i) = legenda(i) + "\quad 0"+round(n(i,j))+"$";
                else
                    legenda(i) = legenda(i) + "\quad  "+round(n(i,j))+"$";
                end
            end
        end
    hold off
    % now it's just more visuals
    ax = utf_axes(gca,32);
    ax.Box = 'on';
    ax.XScale = 'log';
    ax.XTick = [0.25 0.5 1 2];
    ax.XMinorTick = 'on';
    ax.YMinorTick = 'on';
    ax.YMinorGrid = 'on';
    L = legend(legenda,'interpreter','latex','location','northwest');
    L.Title.String = "$\quad\ R/r \quad\ \ n$ ";
    L.Title.Interpreter = 'latex';
    title("Area coverage: $"+cov(j)+"\,\% $");
    xlabel("$r/\lambda\quad(\cdot)$");
    ylabel("$s/\mathrm{max}(s) - 1\quad(\cdot)$");
    % saves the plot in pdf format
    utf_adjust_plot_fine(fig,"fig_cov_"+cov(j),screenSize);
end