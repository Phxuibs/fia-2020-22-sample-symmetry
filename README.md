# FIA 2020-22 - sample symmetry

This project stores and makes publicly all data used in the paper "Sample rotational symmetry in the random-incidence scattering coefficient measurement", presented at XII Congresso Íbero-americano de Acústica, 2022. The paper is available at this [link](https://www.fia2022.com.br/arearestrita/apresentacoes/9032.pdf).

Folder names (and numbers) relate to sections in the paper. The root folder also contains the slides used for the presentation at the congress - they are a good one-line-of-thought summary.

***

## Requirements

All codes are in MATLAB and some make use of the [ITA toolbox](https://www.ita-toolbox.org/index.php) or our own [UTF toolbox](https://gitlab.com/Phxuibs/utf-toolbox.git) - this one may also need ITA's. Functions from these toolboxes have the prefixes `ita_` and `utf_`, respectively.

## Roadmap

No new data will be added to this project. Maybe a video presentation will be added(?). Any new analysis will probably take part on another repository.

## Contributing and Support

This repository is for you, fellow researcher, to use the data, investigate the methods, do your own analysis and be part of the research, so we'll be glad to hear from you any new use of this data.
Therefore, if you need any help or want to share any suggestion, critique or development, don't hesitate to email luis.2014@alunos.utfpr.edu.br.

## Licensing

See the LICENSE.txt file.

## Acknowledgment

Thanks go to all authors: Márcio Gomes, Rodrigo Catai and specially José Larcher, who helped set up this repository.
